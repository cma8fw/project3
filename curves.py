"""
curves.py

UVa cs1120
Project 3

This file provides code for colors, points, and curves.

You should not need to modify this code, but should be
able to understand most (but not all) of it.
"""

import math

# Colors (from Project 1)

### Functions for making colors and extracting their components.

def make_color(red, green, blue):
    """
    We represent a color as a list of three numbers corresponding to its
    red, green and blue components.
    """
    return [red, green, blue]

def get_red(color):
    """Return the red component of the input color."""
    return color[0]

def get_green(color):
    """Return the green component of the input color."""
    return color[1]

def get_blue(color):
    """Return the blue component of the input color."""
    return color[2]

# Here are some common colors:
WHITE = make_color(255, 255, 255)
BLACK = make_color(0, 0, 0)
RED = make_color(255, 0, 0)
GREEN = make_color(0, 255, 0)
BLUE = make_color(0, 0, 255)
YELLOW = make_color(255, 255, 0)

# Support for working with colors:
def dec_to_hex(val):
    """Returns a 2-character hex representation of the input."""
    result = hex(val)[2:]
    if len(result) < 2:
        result = "0" + result
    return result

def convert_color_to_string(color):
    """Returns a hex-string representation of a color."""
    return "#" + ''.join([dec_to_hex(component(color)) 
                          for component in (get_red, get_green, get_blue)])

#
# Points
#

def make_colored_point(x, y, c):
    """Returns a new point at location (x, y) with color c."""
    return [x, y, c]

def make_point(x, y):
    """Returns a new point at location (x, y) with default color BLACK."""
    return make_colored_point(x, y, BLACK)

def point_x(point):
    """Returns the x-coordinate of a point."""
    return point[0]

def point_y(point):
    """Returns the y-coordinate of a point."""
    return point[1]

def point_color(point):
    """Returns the color of a point."""
    return point[2]

# Drawing a curve

def draw_curve_points(curve, npoints):
    """Draws the curve given by the function curve, at npoints + 1 different points."""
    for step in range(npoints + 1):
        window_draw_point(curve(step / npoints))

def draw_curve_connected(curve, npoints):
    """
    Draws the curve given by the function curve, connecting lines
    between n different points.
    """
    for step in range(npoints):
        window_draw_line(curve(step / npoints), curve((step + 1) / npoints))

# Some simple curves

def mid_line(t):
    """Returns a curve that draws a horizontal line in the middle of the window."""
    return make_point(t, .5)

def horiz_line(t):
    """Returns a curve that draws a horizontal line at the bottom of the window."""
    return make_point(t, 0)

def vertical_line(t):
    """Returns a curve that draws a vertical line at the left edge of the window."""
    return make_point(0, t)

def unit_circle(t):
    """Returns a curve that draws a unit circle."""
    # See http://www.mathopenref.com/coordparamcircle.html for math.
    return make_point(math.cos(math.pi * 2 * t), math.sin(math.pi * 2 * t))

# Functions for transforming curves into new curves

def translate(curve, x, y):
    """Returns a new curve that is the input curve translated by (x, y)."""
    def new_curve(t):
        ct = curve(t)
        return make_colored_point(x + point_x(ct), y + point_y(ct), point_color(ct))
    return new_curve

def rotate_ccw(curve):
    """
    Returns a new curve that is the input curve rotated counter-clockwise
    (flipping the x and y coordinates).
    """
    def new_curve(t):
        ct = curve(t)
        return make_colored_point(point_y(ct), point_x(ct), point_color(ct))
    return new_curve

def flip_vertically(curve):
    """
    Returns a new curve that is the input curve flipped over the vertical axis.
    """
    def new_curve(t):
        ct = curve(t)
        return make_colored_point(-1 * point_x(ct), point_y(ct), point_color(ct))
    return new_curve

def first_half(curve):
    """
    Returns a new curve that is the first half (by points) of the input curve.
    """
    def new_curve(t):
        return curve(t / 2)
    return new_curve

# alternate version using lambda to make procedure without definition:
# def first_half(curve):
#     return lambda t: curve(t / 2)

def degrees_to_radians(degrees):
    """Returns the number of radians in the degrees input."""
    return (math.pi * degrees) / 180

#
# rotate-around-origin counterclockwise by theta degrees
# (You do not need to understand the geometry math used here.)
#

def rotate_around_origin(curve, theta):
    """
    Returns a new curve that is the input curve rotated by theta 
    degrees around the origin.
    """
    cth = math.cos(degrees_to_radians(theta))
    sth = math.sin(degrees_to_radians(theta))
    def new_curve(t):
        ct = curve(t)
        x = point_x(ct)
        y = point_y(ct)
        return make_colored_point(x * cth - y * sth, x * sth + y * cth, point_color(ct))
    return new_curve

#
# Scale a curve
#

def scale_x_y(curve, x_scale, y_scale):
    """
    Returns a new curve that is the original curve scaled by x_scale and y_scale.
    """
    def new_curve(t):
        ct = curve(t)
        return make_colored_point(x_scale * point_x(ct), y_scale * point_y(ct), point_color(ct))
    return new_curve

def scale(curve, ratio):
    """
    Returns a new curve that is the original curve scaled by ratio in both
    x and y dimensions.
    """
    return scale_x_y(curve, ratio, ratio)

def squeeze_rectangular_portion(curve, xlo, xhi, ylo, yhi):
    """
    Returns a new curve that is the original curve scaled and
    translated so the portion of the curve in the rectangle
    with corners (xlo, ylo) and (xhi, yhi) will appear in a display 
    window which has x, y coordinates from 0 to 1.
    """
    return scale_x_y(translate(curve, -1 * xlo, -1 * ylo, 1.0/(xhi - xlo), 1.0/(yhi - ylo)))

def connect_rigidly(curve1, curve2):
    """
    Returns a new curve that is curve1 followed by curve2, dividing the 
    points evenly between the two curves.
    """
    def new_curve(t):
        if t < 0.5:
            return curve1(2 * t)
        else:
            return curve2(2 * t - 1)
    return new_curve

def connect_curves_evenly(curvelist):
    """
    Returns a new curve that connects all curves in the curvelist, 
    distributing points evenly among all the curves.
    """
    ncurves = len(curvelist)
    def new_curve(t):
        which_curve = min(int(math.floor(t * ncurves)), ncurves - 1)
        chosen_curve = curvelist[which_curve]
        adjusted_t = ncurves * (t - which_curve * 1.0 / ncurves)
        return chosen_curve(adjusted_t)
    return new_curve

def construct_simple_curvelist(curve, curvelist):
    """
    Returns a list of curves consisting of the input curve, 
    followed by all the curves in curvelist translated to
    the position of the last point in the first input curve.
    """
    endpoint = curve(1.0)
    delta_x = point_x(endpoint)
    delta_y = point_y(endpoint)
    return [curve] + [translate(tcurve, delta_x, delta_y) for tcurve in curvelist]

#
# These procedures find the extents of a curve, so we can scale it to the
# window:
#

def find_extreme_point(curve, point_selector, comparison, n):
    """
    Returns the most extreme point in curve, according to the 
    input comparison, over n points.
    """
    t = 0.0
    step = 1.0 / n
    best_so_far = False
    while t < 1.0:
        if (not best_so_far) or comparison(point_selector(curve(t)), best_so_far):
            best_so_far = point_selector(curve(t))
        t = t + step
    if comparison(point_selector(curve(1.0)), best_so_far):
        return point_selector(curve(1.0))
    else:
        return best_so_far

# Need to define less_than and greater_than functions, since operators cannot be
# passed as parameters.

def less_than(a, b):
    """Returns True iff a is less than b."""
    return a < b

def greater_than(a, b):
    """Returns True iff a is greater than b."""
    return a > b

def find_leftmost_point(curve, npoints):
    """Returns the leftmost point in the curve, when drawing npoints.""" 
    return find_extreme_point(curve, point_x, less_than, npoints)

def find_rightmost_point(curve, npoints):
    """Returns the rightmost point in the curve, when drawing npoints.""" 
    return find_extreme_point(curve, point_x, greater_than, npoints)

def find_lowest_point(curve, npoints):
    """Returns the lowest (vertically) point in the curve, when drawing npoints.""" 
    return find_extreme_point(curve, point_y, less_than, npoints)

def find_highest_point(curve, npoints):
    """Returns the highest point in the curve, when drawing npoints.""" 
    return find_extreme_point(curve, point_y, greater_than, npoints)

def position_curve(curve, startx, starty):
    """
    Returns a new curve that is the input curve, moved to start at 
    (startx, starty), and scaled to fit in the display window.
    """
    tcurve = translate(curve, startx, starty)
    num_points = 1000
    xlo = find_leftmost_point(tcurve, num_points)
    xhi = find_rightmost_point(tcurve, num_points)
    ylo = find_lowest_point(tcurve, num_points)
    yhi = find_highest_point(tcurve, num_points)
    if xlo < 0.01:
        xlowscale = (startx - 0.01)/(startx - xlo)
    else:
        xlowscale = 1.0

    if xhi > 0.99:
        xhighscale = (0.99 - startx)/(xhi - startx)
    else:
        xhighscale = 1.0

    if ylo < 0.01:
        ylowscale = (starty - 0.01)/(starty - ylo)
    else:
        ylowscale = 1.0

    if yhi > 0.99:
        yhighscale = (0.99 - starty)/(yhi - starty)
    else:
        yhighscale = 1.0

    minscale = min(xlowscale, xhighscale, ylowscale, yhighscale)
    return translate(scale_x_y(curve, minscale, minscale), startx, starty)

###
###
### Drawing points and lines
###

# Don't worry about understanding this code, it just draws points and lines on the window.

import turtle
from graphics import gwindow

def window_draw_point(point):
    new_x = int(point_x(point) * gwindow.w - gwindow.h / 2)
    new_y = -1 * int(point_y(point) * gwindow.h - gwindow.h / 2)
    cl = convert_color_to_string(point_color(point))
    gwindow.cv.create_line(new_x, new_y, new_x + 1, new_y - 1, fill = cl)
    turtle.update()

def window_draw_line(point1, point2):
    new_x_1 = int(point_x(point1) * gwindow.w - gwindow.h / 2)
    new_y_1 = -1 * int(point_y(point1) * gwindow.h - gwindow.h / 2)
    new_x_2 = int(point_x(point2) * gwindow.w - gwindow.h / 2)
    new_y_2 = -1*int(point_y(point2) * gwindow.h - gwindow.h / 2)
    gwindow.cv.create_line(new_x_1, new_y_1, new_x_2, new_y_2)
    turtle.update()

