"""
graphics.py

UVa cs1120
Project 3

This file provides code for simple graphics, including drawing with a turtle.

You should not need to modify this code, and are not expected to read it.
"""

import turtle

# The word "class" means that we're using Object-Oriented Programming
# here, a concept that we will cover later in the course. Don't
# worry about "class" or "__init__" for now.

class Graph:
    def __init__(self, width, height):
        self.w = width
        self.h = height
        turtle.setup(self.w, self.h, 0, 0)
        self.cv = turtle.getcanvas()

gwindow = Graph(600, 600) # Default window size is 600x600, can be changed
s = turtle.Screen()

def kill():
    s.bye()

s.onkey(kill, "q")

def save_image(filename):
    from canvasvg import saveall
    turtle.hideturtle()
    ts = turtle.getscreen()
    canvas = ts.getcanvas()
    saveall(filename, canvas)
    # ts.getcanvas().postscript(file=filename)

